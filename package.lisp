;; Copyright 2007-2012 Vladimir Sedach <vas@oneofus.la>

;; SPDX-License-Identifier: LGPL-3.0-or-later

;; This file is part of uri-template.

;; uri-template is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; uri-template is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public
;; License along with uri-template. If not, see
;; <https://www.gnu.org/licenses/>.

(in-package #:cl)

(defpackage #:uri-template
  (:use #:cl #:cl-ppcre #:named-readtables)
  (:export
   ;; common
   #:uri-template

   ;; interpolation
   #:enable-uri-template-syntax
   #:read-uri-template
   #:uri-encode?
   #:uri-encode

   ;; destructuring
   #:uri-template-bind
   #:uri-decode?
   #:uri-decode

   ;; RFC 2396 standard URI components
   #:%uri-scheme
   #:%uri-authority
   #:%uri-path
   #:%uri-query
   #:%uri-fragment

   ;; extended components
   #:%uri-head
   #:%uri-tail
   #:%uri-user
   #:%uri-host
   #:%uri-port
   #:%uri-directory
   #:%uri-file))

(in-package #:uri-template)

(defreadtable uri-template
  (:merge :standard)
  (:dispatch-macro-char #\# #\U
                        (lambda (&rest args)
                          (apply #'uri-template-reader args))))
