;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-

(asdf:defsystem :uri-template.test
  :license "LGPL-3.0-or-later"
  :serial t
  :components ((:file "test-package")
               (:file "uri-template-test"))
  :depends-on (:uri-template :fiveam))
